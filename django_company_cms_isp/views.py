from math import ceil
from django.conf import settings

from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.utils.translation import gettext as _

from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsGeneral,
    CompanyCmsPressReference,
)
from django_company_cms_isp.django_company_cms_isp.forms import AvailabilityCheckForm
from django_company_cms_isp.django_company_cms_isp.func import geoloc_as_str
from django_ispstack_client.django_ispstack_client.func import *
from django_mdat_location.django_mdat_location.models import *


def availability_check(request, country="de", zip_code=0):
    template = loader.get_template("django_company_cms_isp/availability_check.html")

    form_initial = dict()

    template_opts = dict()

    try:
        mdat_country = MdatCountries.objects.get(iso=country)
    except MdatCountries.DoesNotExist:
        mdat_country = MdatCountries.objects.get(iso="de")

    form_initial["country"] = mdat_country

    try:
        mdat_city = MdatCities.objects.get(country=mdat_country, zip_code=zip_code)
    except MdatCities.DoesNotExist:
        mdat_city = None

    template_opts["content_title_main"] = _("Internet Availability Check")

    template_opts["content_title_sub"] = mdat_country.title

    template_opts["isp_product_availability_check_01_header"] = (
        CompanyCmsGeneral.objects.get(
            name="isp_product_availability_check_01_header"
        ).content
    )
    template_opts["isp_product_availability_check_01_content"] = (
        CompanyCmsGeneral.objects.get(
            name="isp_product_availability_check_01_content"
        ).content
    )

    template_opts["isp_product_availability_check_02_header"] = (
        CompanyCmsGeneral.objects.get(
            name="isp_product_availability_check_02_header"
        ).content
    )
    template_opts["isp_product_availability_check_02_content"] = (
        CompanyCmsGeneral.objects.get(
            name="isp_product_availability_check_02_content"
        ).content
    )

    if mdat_city is not None:
        template_opts["content_title_sub"] = (
            str(mdat_city.zip_code) + " " + mdat_city.title + ", " + mdat_country.title
        )
        form_initial["zip_code"] = mdat_city

    # Form
    if request.method == "POST":
        availability_check_form = AvailabilityCheckForm(request.POST)
        if availability_check_form.is_valid():
            request.session["isp_availability_check_last_request"] = request.POST

    # Fetch last search from session for better UX
    isp_availability_check_last_request = request.session.get(
        "isp_availability_check_last_request", None
    )

    if isp_availability_check_last_request is not None:
        # create a form instance and populate it with data from the request:
        availability_check_form = AvailabilityCheckForm(
            isp_availability_check_last_request
        )

        # check whether it's valid:
        if availability_check_form.is_valid():
            # redirect to mdat subpage
            if (
                country != availability_check_form.cleaned_data["country"].iso
                or zip_code != availability_check_form.cleaned_data["zip_code"].zip_code
            ):
                return HttpResponseRedirect(
                    reverse(
                        "isp_internet_check_w_country_a_zip_code",
                        args=(
                            availability_check_form.cleaned_data["country"].iso,
                            availability_check_form.cleaned_data["zip_code"].zip_code,
                        ),
                    )
                )

            # check availability
            available_products = ispstack_internet_availability_check_by_address(
                mdat_id=settings.ISPSTACK_API_USER,
                country_iso=availability_check_form.cleaned_data["country"].iso,
                zip_code=availability_check_form.cleaned_data["zip_code"].zip_code,
                street=availability_check_form.cleaned_data["street"],
                house_nr=availability_check_form.cleaned_data["house_nr"],
            )

            # only show a maximum of four products
            available_products = available_products[:4]

            # TODO: display prices
            for available_product in available_products:
                available_product["price"] = _("price available on request")

            template_opts["product_list"] = available_products

            product_list_count = len(available_products)

            template_opts["product_list_count"] = product_list_count

            if product_list_count > 0:
                template_opts["product_list_split_factor"] = ceil(
                    12 / len(available_products)
                )
            else:
                template_opts["product_list_split_factor"] = 12

    # if a GET (or any other method) we'll create a blank form
    else:
        availability_check_form = AvailabilityCheckForm(initial=form_initial)

    template_opts["availability_check_form"] = availability_check_form

    return HttpResponse(template.render(template_opts, request))


def public_wlan(request, country="de", zip_code=0):
    template = loader.get_template("django_company_cms_isp/public_wlan.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Public WLAN & Guest Access")
    template_opts["content_title_sub"] = _("Secure & Reliable")

    template_opts["isp_public_wlan_01_header"] = CompanyCmsGeneral.objects.get(
        name="isp_public_wlan_01_header"
    ).content
    template_opts["isp_public_wlan_01_content"] = CompanyCmsGeneral.objects.get(
        name="isp_public_wlan_01_content"
    ).content

    # MAP DEFAULTS
    template_opts["map_zoom_level"] = 1
    template_opts["map_geo_location"] = "0,0"

    try:
        mdat_country = MdatCountries.objects.get(iso=country)
    except MdatCountries.DoesNotExist:
        mdat_country = None

    try:
        mdat_city = MdatCities.objects.get(country=mdat_country, zip_code=zip_code)
    except MdatCities.DoesNotExist:
        mdat_city = None

    if mdat_country is not None:
        template_opts["content_title_sub"] = mdat_country.title

        if mdat_country.geoloc:
            if "coordinates" in mdat_country.geoloc:
                template_opts["map_geo_location"] = geoloc_as_str(mdat_country.geoloc)
                template_opts["map_zoom_level"] = 5

    if mdat_city is not None:
        template_opts["content_title_sub"] = mdat_city.title

        if mdat_city.geoloc:
            if "coordinates" in mdat_city.geoloc:
                template_opts["map_geo_location"] = geoloc_as_str(mdat_city.geoloc)
                template_opts["map_zoom_level"] = 13

    # References
    reference_list = CompanyCmsPressReference.objects.filter(topic__name="public_wlan")
    template_opts["reference_list"] = reference_list

    # Products
    template_opts["product_comparison_title"] = _("Available Products")
    template_opts["product_comparison_subtitle"] = (
        _("Maybe these products are of interest") + ":"
    )
    available_products = list()

    # individual
    available_products.append(
        {
            "name": _("Premium Access"),
            "price": _("price available on request"),
            "text": _("Perfect solution for individuals, hotels and restaurants."),
        }
    )

    # community
    available_products.append(
        {
            "name": _("Community Contract"),
            "price": _("price available on request"),
            "text": _(
                "Perfect solution for the public sector using a central contract. Allows individuals and vendors of the same area to host hotspots for free."
            ),
        }
    )

    template_opts["product_list"] = available_products

    product_list_count = len(available_products)

    template_opts["product_list_count"] = product_list_count

    if product_list_count > 0:
        template_opts["product_list_split_factor"] = ceil(12 / len(available_products))
    else:
        template_opts["product_list_split_factor"] = 12

    return HttpResponse(template.render(template_opts, request))
