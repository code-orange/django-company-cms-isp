from django.urls import path

from . import views

urlpatterns = [
    path("internet/check", views.availability_check, name="isp_internet_check"),
    path(
        "internet/check/<str:country>",
        views.availability_check,
        name="isp_internet_check_w_country",
    ),
    path(
        "internet/check/<str:country>/<str:zip_code>",
        views.availability_check,
        name="isp_internet_check_w_country_a_zip_code",
    ),
    path("public-wlan", views.public_wlan, name="isp_public_wlan"),
    path(
        "public-wlan/<str:country>", views.public_wlan, name="isp_public_wlan_w_country"
    ),
    path(
        "public-wlan/<str:country>/<str:zip_code>",
        views.public_wlan,
        name="isp_public_wlan_w_country_a_zip_code",
    ),
]
