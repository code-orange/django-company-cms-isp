from datetime import datetime

from django.contrib.sitemaps import Sitemap
from django.urls import reverse

from django_mdat_location.django_mdat_location.models import *
from . import views


class IspStaticViewSitemap(Sitemap):
    def items(self):
        return ["isp_internet_check", "isp_public_wlan"]

    def location(self, item):
        return reverse(item)


class IspAvailabilityCheckCountriesSitemap(Sitemap):
    def items(self):
        return MdatCountries.objects.all().select_related()

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return reverse(views.availability_check, args=[obj.iso.lower()])


class IspAvailabilityCheckCitiesSitemap(Sitemap):
    def items(self):
        return MdatCities.objects.all().select_related()

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return reverse(
            views.availability_check,
            args=[obj.country.iso.lower(), obj.zip_code.lower()],
        )


class IspPublicWlanCountriesSitemap(Sitemap):
    def items(self):
        return MdatCountries.objects.all().select_related()

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return reverse(views.public_wlan, args=[obj.iso.lower()])


class IspPublicWlanCitiesSitemap(Sitemap):
    def items(self):
        return MdatCities.objects.all().select_related()

    def lastmod(self, obj):
        return datetime.now()

    def location(self, obj):
        return reverse(
            views.public_wlan, args=[obj.country.iso.lower(), obj.zip_code.lower()]
        )
