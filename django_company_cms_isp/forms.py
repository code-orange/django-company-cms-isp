from dal import autocomplete
from django import forms
from django.utils.translation import gettext_lazy as _

from django_mdat_location.django_mdat_location.models import *


class AvailabilityCheckForm(forms.Form):
    country = forms.ModelChoiceField(
        label=_("Country"),
        queryset=MdatCountries.objects.all().select_related(),
        widget=autocomplete.ModelSelect2(
            url="mdat-countries-autocomplete",
            attrs={
                "data-placeholder": _("Country"),
                "placeholder": _("Country"),
                "class": "form-control custom-select",
            },
        ),
    )

    zip_code = forms.ModelChoiceField(
        label=_("ZIP Code"),
        queryset=MdatCities.objects.all().select_related(),
        widget=autocomplete.ModelSelect2(
            url="mdat-cities-autocomplete",
            forward=["country"],
            attrs={
                "data-placeholder": _("ZIP Code"),
                "placeholder": _("ZIP Code"),
                "class": "form-control custom-select",
            },
        ),
    )

    street = forms.CharField(
        label=_("Street"),
        max_length=100,
        widget=forms.TextInput(
            attrs={
                "data-placeholder": _("Street"),
                "placeholder": _("Street"),
                "class": "form-control",
            }
        ),
    )

    house_nr = forms.CharField(
        label=_("House Number"),
        max_length=10,
        widget=forms.TextInput(
            attrs={
                "data-placeholder": _("House Number"),
                "placeholder": _("House Number"),
                "class": "form-control",
            }
        ),
    )
